import re
from dataclasses import dataclass
from itertools import product
from pathlib import Path
from sys import argv
from time import perf_counter
from typing import Callable, Any, List, Dict, Literal

import yaml
from yachalk import chalk

from .chalk_cycler import ChalkCycler
from .read_file import read_file


@dataclass
class Run:
    fn: Callable[..., Any]
    inputs: List[List[str]]

    @staticmethod
    def from_config(conf: List[List[str]], fn: Callable[..., Any]):
        return Run(fn=fn, inputs=conf)


Inputs = Dict[str, Any]
Runs = Dict[str, Run]
RunResults = Dict[str, Dict[str, Dict[Literal['result', 'time'], Any]]]


@dataclass
class RunConfig:
    inputs: Inputs
    runs: Runs

    @staticmethod
    def from_config(conf: Dict[str, Any], fns: Dict[str, Callable[..., Any]]):
        return RunConfig(
            conf['inputs'],
            {name: Run.from_config(run, fns[name]) for name, run in conf['runs'].items()}
        )

    @classmethod
    def from_yaml(cls, path: str, fns: Dict[str, Callable[..., Any]]):
        with open(Path(argv[0]).parent.joinpath(path), 'r') as f:
            raw = yaml.safe_load(f)
            parsed = cls.from_config(raw, fns)
        return parsed


def compile_arg(arg: str) -> re.Pattern:
    if arg.startswith('re:'):
        return re.compile(arg.removeprefix('re:'))
    return re.compile(f'^{arg}$')


def run_from_config(conf: RunConfig) -> RunResults:
    run_colors = ChalkCycler.randomized()
    run_results: RunResults = {}
    for name, run in conf.runs.items():
        next_color = run_colors.next
        run_results[name] = {}
        run_dict = run_results[name]
        for args in run.inputs:
            re_arg_name = [compile_arg(arg) for arg in args]
            arg_name_combinations = list(product(
                *([key for key in conf.inputs if pattern.search(key)] for pattern in re_arg_name)
            ))
            for arg_names in arg_name_combinations:
                # TODO could also abstract out the printing process instead of handling it here
                print(next_color(f'{name}({chalk.reset(", ".join(arg_names))}) -> '), end='')
                time = perf_counter()
                # just in case arguments are from a file, load said file first
                # TODO this would be a good spot for a strategy/plugin pattern for auto-loading data into data structs
                # e.g. csv_ints:, csv:, ints:, floats:, etc.
                for key in arg_names:
                    value: Any = conf.inputs.get(key)
                    if isinstance(value, str) and value.startswith('file:'):
                        time = perf_counter()
                        conf.inputs[key] = read_file(value.removeprefix('file:'))
                        print(f'{perf_counter() - time:.3f}s')
                result = run.fn(*(conf.inputs.get(name) for name in arg_names))
                time = perf_counter() - time
                print(f'{result} ... {time:.3f}s')
                run_dict[', '.join(arg_names)] = {'result': result, 'time': time}
    return run_results


def save_run_results(path: str, run_results: RunResults) -> None:
    with open(Path(argv[0]).parent.joinpath(path), 'w') as f:
        yaml.safe_dump(run_results, f)
