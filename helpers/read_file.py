from pathlib import Path
from sys import argv


def read_file(filename: str) -> str:
    with Path(argv[0]).parent.joinpath(filename).open('r', encoding='utf8') as f:
        return f.read()
