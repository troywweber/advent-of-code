from pathlib import Path
from sys import argv
from typing import List


def read_input(filename: str) -> List[str]:
    with Path(argv[0]).parent.joinpath(filename).open('r', encoding='utf8') as f:
        return [line.rstrip() for line in f]
