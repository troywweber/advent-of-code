from dataclasses import dataclass, astuple
from itertools import chain
from time import perf_counter
from typing import Callable, Any, Dict, Tuple


@dataclass
class LogTimeData:
    name: str
    args: Tuple[Any, ...]
    kwargs: Dict[str, Any]
    result: Any
    time: float


LogTimeFormatter = Callable[[LogTimeData], str]


def default_formatter(data: LogTimeData) -> str:
    name, args, kwargs, result, time = astuple(data)
    param_str: str = ','.join(chain((str(arg) for arg in args), (f'{key}={kwargs[key]}' for key in kwargs)))
    return f'{name}({param_str}) -> {result} ... {time:.3f}s'


def ignore_args_formatter(data: LogTimeData) -> str:
    name, _, _, result, time = astuple(data)
    return f'{name} -> {result} ... {time:.3f}s'


def log_time(formatter: LogTimeFormatter = None) -> Callable[[Callable], Callable]:
    if formatter is None:
        formatter = default_formatter

    def decorator(fn: Callable) -> Callable:
        def wrapper(*args: Tuple[Any, ...], **kwargs: Dict[str, Any]) -> Any:
            time: float = perf_counter()
            result: Any = fn(*args, **kwargs)
            time = perf_counter() - time
            print(formatter(LogTimeData(fn.__name__, args, kwargs, result, time)))
            return result

        return wrapper

    return decorator
