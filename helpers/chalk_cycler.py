from dataclasses import dataclass, field
from random import shuffle
from typing import List, Optional

from yachalk import chalk
from yachalk.chalk_builder import ChalkBuilder


@dataclass
class ChalkCycler:
    __colors: List[ChalkBuilder]
    __index: int = field(init=False, default=0)

    @property
    def __increment(self):
        index = self.__index
        self.__index += 1
        self.__index %= len(self.__colors)
        return index

    @property
    def next(self) -> ChalkBuilder:
        return self.__colors[self.__increment]

    @staticmethod
    def randomized(colors: Optional[List[ChalkBuilder]] = None):
        if colors is None:
            colors = [chalk.red, chalk.red_bright, chalk.yellow, chalk.yellow_bright, chalk.green, chalk.green_bright,
                      chalk.blue, chalk.blue_bright, chalk.magenta, chalk.magenta_bright, chalk.grey]
        shuffle(colors)
        return ChalkCycler(colors)
