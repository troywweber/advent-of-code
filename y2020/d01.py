#!/usr/bin/env python

from typing import List

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


@log_time(ignore_args_formatter)
def part1(d: List[int]) -> int:
    num_list: List[int] = []
    for value in d:
        partner = 2020 - value
        if partner in num_list:
            return value * partner
        num_list.append(value)


@log_time(ignore_args_formatter)
def part2(d: List[int]) -> int:
    num_list: List[int] = []
    for first in d:
        remainder = 2020 - first
        for value in num_list:
            final = remainder - value
            if final in num_list:
                return first * value * final
        else:
            num_list.append(first)


if __name__ == '__main__':
    data: List[int] = [int(value) for value in read_input('d01_in.txt')]
    part1(data)
    part2(data)
