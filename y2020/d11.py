#!/usr/bin/env pythonfrom collections import defaultdict, Counter

from collections import defaultdict, Counter
from copy import copy
from typing import Tuple, DefaultDict, List, Iterable

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input

SeatGrid = DefaultDict[Tuple[int, int], str]
FLOOR = '.'
EMPTY = 'L'
TAKEN = '#'


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    seats: SeatGrid = defaultdict(lambda: FLOOR)
    n_rows = len(d)
    n_cols = len(d[0])
    for i in range(n_rows):
        for j in range(n_cols):
            seats[i, j] = d[i][j]
    seats_copy: SeatGrid = copy(seats)
    addresses = [a for a in seats if seats[a] != FLOOR]
    changed = True

    def iter_adjacent(s: SeatGrid, a: Tuple[int, int]) -> Iterable[str]:
        for r in range(a[0] - 1, a[0] + 2):
            for c in range(a[1] - 1, a[1] + 2):
                if (r, c) != a:
                    yield s[r, c]

    while changed:
        changed = False
        for addr in addresses:
            adj = Counter(iter_adjacent(seats, addr))
            if seats[addr] == EMPTY:
                if adj[TAKEN] == 0:
                    changed = True
                    seats_copy[addr] = TAKEN
            else:
                if adj[TAKEN] >= 4:
                    changed = True
                    seats_copy[addr] = EMPTY
        seats, seats_copy = seats_copy, copy(seats_copy)

    count = Counter(seats[addr] for addr in addresses)
    return f'There are {count[TAKEN]} seats occupied'


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    seats: SeatGrid = defaultdict(lambda: FLOOR)
    n_rows = len(d)
    n_cols = len(d[0])
    for i in range(n_rows):
        for j in range(n_cols):
            seats[i, j] = d[i][j]
    seats_copy: SeatGrid = copy(seats)
    addresses = [a for a in seats if seats[a] != FLOOR]
    changed = True

    def iter_sight_addresses(sight: Tuple[int, int]) -> Iterable[Tuple[int, int]]:
        r, c = sight
        magnitude = 0
        while True:
            magnitude += 1
            yield r * magnitude, c * magnitude

    def iter_adjacent(s: SeatGrid, a: Tuple[int, int]) -> Iterable[str]:
        for r in range(-1, 2):
            for c in range(-1, 2):
                if (r, c) != (0, 0):
                    for sight in iter_sight_addresses((r, c)):
                        r_seat = a[0] + sight[0]
                        c_seat = a[1] + sight[1]
                        if r_seat < 0 or n_rows <= r_seat or c_seat < 0 or n_cols <= c_seat:
                            break
                        elif s[r_seat, c_seat] != FLOOR:
                            yield s[r_seat, c_seat]
                            break

    while changed:
        changed = False
        for addr in addresses:
            adj = Counter(iter_adjacent(seats, addr))
            if seats[addr] == EMPTY:
                if adj[TAKEN] == 0:
                    changed = True
                    seats_copy[addr] = TAKEN
            else:
                if adj[TAKEN] >= 5:
                    changed = True
                    seats_copy[addr] = EMPTY
        seats, seats_copy = seats_copy, copy(seats_copy)

    count = Counter(seats[addr] for addr in addresses)
    return f'There are {count[TAKEN]} seats occupied'


if __name__ == '__main__':
    data: List[str] = read_input('d11_in.txt')
    part1(data)
    part2(data)
