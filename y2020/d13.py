#!/usr/bin/env python

from typing import List, Tuple

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


@log_time(ignore_args_formatter)
def part1(d: List[str]):
    depart_time: int = int(d[0])
    bus_ids: List[int] = [int(x) for x in filter(lambda x: x != 'x', d[1].split(','))]
    bus_waits: List[int] = [bid - depart_time % bid for bid in bus_ids]
    min_wait: int = min(bus_waits)
    bus_id: int = bus_ids[bus_waits.index(min_wait)]
    return f'Bus id is {bus_id}, wait time is {min_wait}, and product is {bus_id * min_wait}'


@log_time(ignore_args_formatter)
def part2(d: List[str]):
    bus_data: List[Tuple[int, int]] = [(int(bid), td) for td, bid in enumerate(d[1].split(',')) if bid != 'x']
    t = 0
    step = 1
    for bid, td in bus_data:
        r = (bid - td) % bid
        while t % bid != r:
            t += step
        step *= bid
    return f'After {t}, all busses depart according to their offset'


if __name__ == '__main__':
    data: List[str] = read_input('d13_in.txt')
    part1(data)
    part2(data)
