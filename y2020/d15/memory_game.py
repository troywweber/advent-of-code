#!/usr/bin/env python

from typing import List, Dict


def memory_game(starters: List[int], final_n: int) -> int:
    memory: Dict[int, int] = {v: i + 1 for i, v in enumerate(starters)}
    last_value: int = starters[-1]
    for i in range(len(starters), final_n):
        memory[last_value], last_value = i, i - memory.get(last_value, i)
    return last_value


def memory_game_result(starters_csv: str, n: int) -> str:
    starters = list(int(value) for value in starters_csv.split(','))
    return f'At {n=} the value is {memory_game(starters, n)}'
