from helpers.run_from_config import RunConfig, run_from_config, save_run_results
from y2020.d15.memory_game import memory_game_result


def main():
    context = {
        'Part1': memory_game_result,
        'Part2': memory_game_result
    }
    config = RunConfig.from_yaml('run_config.yml', context)
    run_results = run_from_config(config)
    save_run_results('run_results.yml', run_results)


if __name__ == '__main__':
    main()
