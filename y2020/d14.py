#!/usr/bin/env python
import re
from collections import Counter
from itertools import product
from typing import List, Iterable

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input

RE_MEM = re.compile(r'^mem\[(\d+)]$')


@log_time(ignore_args_formatter)
def part1(d: List[str]):
    def apply_mask(val: int, msk: str) -> int:
        return int(''.join(((b if m == 'X' else m) for b, m in zip(f'{val:036b}', msk))), 2)

    mem = Counter()
    mask: str = ''

    for line in d:
        action, assignment = line.split(' = ')
        if action == 'mask':
            mask = assignment
        else:
            addr = int(RE_MEM.search(action)[1])
            value = apply_mask(int(assignment), mask)
            mem[addr] = value
    return f'The sum of all values in memory is {sum(mem.values())}'


@log_time(ignore_args_formatter)
def part2(d: List[str]):
    def floating_address(addr: int, msk: str) -> str:
        return ''.join(b if m == '0' else m for b, m in zip(f'{addr:036b}', msk))

    def iter_addresses(addr: str) -> Iterable[int]:
        for digits in product((0, 1), repeat=Counter(addr)['X']):
            values: List[int] = list(digits)
            yield int(''.join(c if c != 'X' else str(values.pop()) for c in addr), 2)

    mem = Counter()
    mask: str = ''

    for line in d:
        action, assignment = line.split(' = ')
        if action == 'mask':
            mask = assignment
        else:
            floating_addr = floating_address(int(RE_MEM.search(action)[1]), mask)
            for address in iter_addresses(floating_addr):
                mem[address] = int(assignment)
    return f'The sum of all values in memory is {sum(mem.values())}'


if __name__ == '__main__':
    data: List[str] = read_input('d14_in.txt')
    part1(data)
    part2(data)
