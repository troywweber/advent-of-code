#!/usr/bin/env python

from typing import List, Iterable, Tuple

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


def iter_preamble_n(preamble_length: int, iter_nums: Iterable[int]) -> Iterable[Tuple[List[int], int]]:
    fifo_buffer: List[int] = []
    for n in iter_nums:
        if len(fifo_buffer) < preamble_length:
            fifo_buffer.append(n)
        else:
            yield list(fifo_buffer), n
            fifo_buffer.pop(0)
            fifo_buffer.append(n)


def can_sum_to_n(values: List[int], n: int) -> bool:
    while values:
        v = values.pop()
        if n - v in values:
            return True
    return False


def find_xmas_break(preamble_length: int, values: Iterable[int]) -> int:
    for preamble, n in iter_preamble_n(preamble_length, values):
        if not can_sum_to_n(preamble, n):
            return n


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    num = find_xmas_break(25, (int(line) for line in d))
    return f'The first number that breaks XMAS is {num}'


def find_contiguous_values(nums: Iterable[int], target: int) -> List[int]:
    fifo_buffer: List[int] = []
    for n in nums:
        while sum(fifo_buffer) > target:
            fifo_buffer.pop(0)
        buffer_sum = sum(fifo_buffer)
        if buffer_sum == target:
            return fifo_buffer
        fifo_buffer.append(n)


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    values: List[int] = list(int(line) for line in d)
    num = find_xmas_break(25, values)
    contiguous_values = find_contiguous_values(values, num)
    final_sum = min(contiguous_values) + max(contiguous_values)
    return f'{contiguous_values} sum to {num}, and the min and max sum to {final_sum}'


if __name__ == '__main__':
    data = read_input('d09_in.txt')
    part1(data)
    part2(data)
