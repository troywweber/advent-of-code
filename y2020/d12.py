#!/usr/bin/env python

from __future__ import annotations

import re
from typing import List, NamedTuple, Dict, Iterable

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


class Vector(NamedTuple):
    x: int
    y: int

    def __add__(self, other: Vector) -> Vector:
        return Vector(self.x + other.x, self.y + other.y)

    def __mul__(self, scale: int) -> Vector:
        return Vector(self.x * scale, self.y * scale)

    def rotate(self, deg: int):
        x, y = self
        deg %= 360
        # (1,2)
        if deg == 90:
            # (-2,1)
            return Vector(-y, x)
        elif deg == 180:
            # (-1,-2)
            return Vector(-x, -y)
        elif deg == 270:
            # (2,-1)
            return Vector(y, -x)
        else:
            return self

    def manhattan_dist(self) -> int:
        return abs(self.x) + abs(self.y)


class Action(NamedTuple):
    cmd: str
    value: int

    @staticmethod
    def parse(value: str) -> Action:
        m = re.search(r'^([A-Z])(\d+)$', value)
        return Action(m[1], int(m[2]))


@log_time(ignore_args_formatter)
def part1(d: List[str]):
    class Ferry:
        HEADING: Dict[str, int] = {'E': 0, 'N': 90, 'W': 180, 'S': 270, }
        DIR: Dict[int, int] = {0: 1, 90: 1, 180: -1, 270: -1}
        heading: int = HEADING['E']
        x: int = 0
        y: int = 0

        def move(self, a: Action):
            cmd, value = a
            if cmd == 'L':
                self.heading = (self.heading + value) % 360
            elif cmd == 'R':
                self.heading = (self.heading - value) % 360
            else:
                heading = self.heading if cmd == 'F' else Ferry.HEADING[cmd]
                magnitude = value * Ferry.DIR[heading]
                if heading == 0 or heading == 180:
                    self.x += magnitude
                else:
                    self.y += magnitude

        def manhattan_dist(self) -> int:
            return abs(self.x) + abs(self.y)

        def __str__(self):
            return f'Ferry(heading={self.heading},x={self.x},y={self.y})'

    instructions: Iterable[Action] = (Action.parse(line) for line in d)
    ferry = Ferry()
    for action in instructions:
        ferry.move(action)
    return f'The manhattan distance between the ferry and the starting point is {ferry.manhattan_dist()}'


@log_time(ignore_args_formatter)
def part2(d: List[str]):
    class Ferry:
        HEADING: Dict[str, Vector] = {
            'E': Vector(1, 0),
            'N': Vector(0, 1),
            'W': Vector(-1, 0),
            'S': Vector(0, -1),
        }
        position = Vector(0, 0)
        heading = Vector(10, 1)

        def run(self, a: Action):
            cmd, value = a
            if cmd == 'F':
                self.position += self.heading * value
            elif cmd == 'R':
                self.heading = self.heading.rotate(-value)
            elif cmd == 'L':
                self.heading = self.heading.rotate(value)
            else:
                self.heading += Ferry.HEADING[cmd] * value

        def manhattan_dist(self) -> int:
            return self.position.manhattan_dist()

        def __str__(self):
            return f'Ferry(position={self.position},heading={self.heading})'

    instructions: Iterable[Action] = (Action.parse(line) for line in d)
    ferry = Ferry()
    for action in instructions:
        ferry.run(action)
    return f'The manhattan distance between the ferry and the starting point is {ferry.manhattan_dist()}'


if __name__ == '__main__':
    data: List[str] = read_input('d12_in.txt')
    part1(data)
    part2(data)
