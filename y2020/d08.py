#!/usr/bin/env python

import re
from typing import Tuple, Set, List

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input

Instruction = Tuple[str, int]
Instructions = List[Instruction]

_RE_INSTRUCTION: re.Pattern = re.compile(r'^(?P<cmd>[a-z]+) (?P<value>[+-]\d+)$')


class InstructionParser(object):
    accumulator: int

    @staticmethod
    def parse_instruction(line: str) -> Instruction:
        match = _RE_INSTRUCTION.search(line).groupdict()
        return match['cmd'], int(match['value'])

    def run(self, instructions: Instructions) -> bool:
        self.accumulator = 0
        run_index: int = 0
        run_memory: Set[int] = set()
        while run_index not in run_memory:
            if run_index >= len(instructions):
                return True
            run_memory.add(run_index)
            cmd, value = instructions[run_index]
            if cmd != 'nop' and cmd != 'acc' and cmd != 'jmp':
                raise ValueError(f'unrecognized command {repr(cmd)}')
            run_index += 1 if cmd == 'nop' or cmd == 'acc' else value
            if cmd == 'acc':
                self.accumulator += value
        return False


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    parser = InstructionParser()
    instructions: Instructions = [InstructionParser.parse_instruction(line) for line in d]
    parser.run(instructions)
    return f'Before looping, the accumulator is {parser.accumulator}'


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    parser = InstructionParser()
    instructions: Instructions = [InstructionParser.parse_instruction(line) for line in d]
    for i, instruction in enumerate(instructions):
        cmd, value = instruction
        if cmd == 'nop' or cmd == 'jmp':
            instructions[i] = ('nop' if cmd == 'jmp' else 'jmp', value)
            if parser.run(instructions):
                break
            instructions[i] = instruction
    return f'The program completes with an accumulator of {parser.accumulator}'


if __name__ == '__main__':
    data = read_input('d08_in.txt')
    part1(data)
    part2(data)
