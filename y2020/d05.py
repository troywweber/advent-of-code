#!/usr/bin/env python

from typing import List, Tuple, Iterable, Optional

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input

RowCol = Tuple[int, int]


def decode_binary(d: str, high_value: str) -> int:
    result = 0
    for c in d:
        result = result << 1
        if c == high_value:
            result += 1
    return result


def get_row_col(packing_id: str) -> RowCol:
    return \
        decode_binary(packing_id[0:7], 'B'), \
        decode_binary(packing_id[7:], 'R')


def get_seat_id(row: int, col: int) -> int:
    return row * 8 + col


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    iter_row_col: Iterable[RowCol] = (get_row_col(datum) for datum in d)
    iter_seat_id: Iterable[int] = (get_seat_id(row, col) for row, col in iter_row_col)
    max_seat_id = max(iter_seat_id)
    return f'highest seat ID is {max_seat_id}'


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    iter_row_col: Iterable[RowCol] = (get_row_col(datum) for datum in d)
    iter_seat_id: Iterable[int] = (get_seat_id(row, col) for row, col in iter_row_col)
    last_id: Optional[int] = None
    for seat_id in sorted(iter_seat_id):
        if last_id is None or seat_id - last_id == 1:
            last_id = seat_id
        else:
            return f'my seat is {seat_id - 1}'


if __name__ == '__main__':
    data = read_input('d05_in.txt')
    part1(data)
    part2(data)
