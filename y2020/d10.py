#!/usr/bin/env python
from collections import Counter
from typing import List

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


def get_input(d: List[str]) -> List[int]:
    values = sorted(int(line) for line in d)
    return [0] + values + [values[-1] + 3]


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    # Improvements made courtesy of https://www.reddit.com/r/adventofcode/comments/ka8z8x/2020_day_10_solutions/
    adapters = get_input(d)
    jolts = Counter()
    for a, b in zip(adapters[1:], adapters):
        jolts[a - b] += 1
    return f'The product of 1-jolt and 3-jolt differences is {jolts[1] * jolts[3]}'


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    # Solution adapted from https://www.reddit.com/r/adventofcode/comments/ka8z8x/2020_day_10_solutions/
    adapters = get_input(d)
    combinations = Counter([0])
    for a in adapters:
        combinations[a] += sum(combinations[a - x] for x in range(1, 4))
    return f'There are {combinations[adapters[-1]]} valid combinations'


if __name__ == '__main__':
    data = read_input('d10_in.txt')
    part1(data)
    part2(data)
