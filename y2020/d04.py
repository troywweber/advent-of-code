#!/usr/bin/env python

import re
from itertools import chain
from typing import List, Iterable, Tuple, Dict, Set

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


def parse_passport(iter_data: Iterable[str]) -> Iterable[Dict[str, str]]:
    passport = dict()
    for chunk in iter_data:
        if chunk != '':
            for key, value in (tuple(entry.split(':') for entry in chunk.split(' '))):
                passport[key] = value
        else:
            yield passport
            passport = dict()


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    n_valid = 0
    for pp in parse_passport(chain(d, [''])):
        if has_required_fields(pp):
            n_valid += 1
    return f'processed {n_valid} valid passports'


_RE_HAIR_COLOR: re.Pattern = re.compile(r'^#[0-9a-f]{6}$')
_RE_HEIGHT: re.Pattern = re.compile(r'^(?P<value>\d+)(?P<units>cm|in)$', re.I)
_RE_PID: re.Pattern = re.compile(r'^\d{9}$')
_REQUIRED_FIELDS: Tuple[str, ...] = ('byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid')
_VALID_EYE_COLORS: Set[str] = {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'}


def has_required_fields(passport: Dict[str, str]) -> bool:
    return all(field in passport for field in _REQUIRED_FIELDS)


def years_valid(birth: str, issued: str, expires: str) -> bool:
    return 1920 <= int(birth) <= 2002 and \
           2010 <= int(issued) <= 2020 <= int(expires) <= 2030


def height_valid(height: str) -> bool:
    m = _RE_HEIGHT.search(height)
    if m:
        groups = m.groupdict()
        value = int(groups['value'])
        units = groups['units']
        return 150 <= value <= 193 \
            if units == 'cm' \
            else 59 <= value <= 76
    else:
        return False


def hair_color_valid(hair_color: str) -> bool:
    return bool(_RE_HAIR_COLOR.search(hair_color))


def eye_color_valid(eye_color: str) -> bool:
    return eye_color in _VALID_EYE_COLORS


def passport_id_valid(pid: str) -> bool:
    return bool(_RE_PID.search(pid))


def passport_valid(passport: Dict[str, str]) -> bool:
    return has_required_fields(passport) \
           and years_valid(passport['byr'], passport['iyr'], passport['eyr']) \
           and height_valid(passport['hgt']) \
           and hair_color_valid(passport['hcl']) \
           and eye_color_valid(passport['ecl']) \
           and passport_id_valid(passport['pid'])


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    n_valid = 0
    for pp in parse_passport(chain(d, [''])):
        if passport_valid(pp):
            n_valid += 1
    return f'processed {n_valid} valid passports'


if __name__ == '__main__':
    data: List[str] = read_input('d04_in.txt')
    part1(data)
    part2(data)
