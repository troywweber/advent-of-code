#!/usr/bin/env python
from itertools import chain
from typing import List, Iterable, Set, Optional

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    def parse() -> Iterable[Set[str]]:
        group_answers = set()
        for chunk in chain(d, ['']):
            if chunk != '':
                for letter in chunk:
                    group_answers.add(letter)
            else:
                yield group_answers
                group_answers = set()

    sum_of_answers = 0
    for answer_set in parse():
        sum_of_answers += len(answer_set)
    return f'The sum of answers is {sum_of_answers}'


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    def parse() -> Iterable[Set[str]]:
        intersection: Optional[Set[str]] = None
        for chunk in chain(d, ['']):
            if chunk != '':
                person_answers: Set[str] = set()
                for letter in chunk:
                    person_answers.add(letter)
                intersection = person_answers if intersection is None else intersection & person_answers
            else:
                yield intersection
                intersection = None

    sum_of_answers = 0
    for answer_set in parse():
        sum_of_answers += len(answer_set)
    return f'The sum of answers is {sum_of_answers}'


if __name__ == '__main__':
    data = read_input('d06_in.txt')
    part1(data)
    part2(data)
