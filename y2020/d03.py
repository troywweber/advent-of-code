#!/usr/bin/env python
from math import prod
from typing import List, Tuple

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


class TreeCounter(object):
    _pattern_width = None
    _x_pos: List[int]
    _y_pos: int
    _slopes: List[Tuple[int, int]]
    _tree: str
    n_trees: List[int]

    def __init__(self, tree: str, slopes: List[Tuple[int, int]]):
        self._tree = tree
        self._slopes = slopes
        n_slopes = len(slopes)
        self._x_pos = [0] * n_slopes
        self._y_pos = 0
        self.n_trees = [0] * n_slopes

    def process(self, data: str):
        if self._pattern_width is None:
            self._pattern_width = len(data)
        for i, (m_x, m_y) in enumerate(self._slopes):
            if self._y_pos % m_y == 0:
                if data[self._x_pos[i]] == self._tree:
                    self.n_trees[i] += 1
                self._x_pos[i] = (self._x_pos[i] + m_x) % self._pattern_width
        self._y_pos += 1


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    counter = TreeCounter('#', [(3, 1)])
    for row in d:
        counter.process(row)
    return f'encountered {sum(counter.n_trees)} trees'


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    counter = TreeCounter('#', [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2), ])
    for row in d:
        counter.process(row)
    return f'encountered {counter.n_trees} trees; product is {prod(counter.n_trees)}'


if __name__ == '__main__':
    data: List[str] = read_input('d03_in.txt')
    part1(data)
    part2(data)
