#!/usr/bin/env python
import re
from typing import List, Dict, Tuple

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input

RE_OUTER_BAG: re.Pattern = re.compile(r'^([a-z]+ [a-z]+) bags? contain')
RE_INNER_BAG: re.Pattern = re.compile(r'(\d+) ([a-z]+ [a-z]+) bags?')
TARGET_BAG: str = 'shiny gold'


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    bag_connections: Dict[str, List[Tuple[str, int]]] = {}
    for rule in d:
        outer = RE_OUTER_BAG.search(rule).groups()[0]
        inners = RE_INNER_BAG.findall(rule)
        for n, inner in inners:
            inner_with_qty: Tuple[str, int] = (inner, int(n))
            if outer in bag_connections:
                bag_connections[outer].append(inner_with_qty)
            else:
                bag_connections[outer] = [inner_with_qty]
    bag_options = 0
    for outer in bag_connections:
        if outer != TARGET_BAG and outer in bag_connections:
            bag_queue: List[str] = [inner for inner, _ in bag_connections[outer]]
            while bag_queue:
                bag = bag_queue.pop(0)
                if bag == TARGET_BAG:
                    bag_options += 1
                    break
                elif bag in bag_connections:
                    bag_queue.extend(inner for inner, _ in bag_connections[bag])
    return f'There are {bag_options} bags that can hold a {TARGET_BAG} bag'


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    bag_connections: Dict[str, List[Tuple[str, int]]] = {}
    for rule in d:
        outer = RE_OUTER_BAG.search(rule).groups()[0]
        inners = RE_INNER_BAG.findall(rule)
        for n, inner in inners:
            inner_with_qty: Tuple[str, int] = (inner, int(n))
            if outer in bag_connections:
                bag_connections[outer].append(inner_with_qty)
            else:
                bag_connections[outer] = [inner_with_qty]
    n_bags = 0
    bag_queue: List[Tuple[str, int]] = [*bag_connections[TARGET_BAG]]
    while bag_queue:
        inner, num = bag_queue.pop(0)
        n_bags += num
        if inner in bag_connections:
            inners = bag_connections[inner]
            bag_queue.extend((bag, n * num) for bag, n in inners)
    return f'My {TARGET_BAG} bag must hold {n_bags} bags'


if __name__ == '__main__':
    data = read_input('d07_in.txt')
    part1(data)
    part2(data)
