#!/usr/bin/env python

from re import Pattern, compile, Match
from typing import List

from helpers.log_time import log_time, ignore_args_formatter
from helpers.read_input import read_input


@log_time(ignore_args_formatter)
def part1(d: List[str]) -> str:
    re_datum: Pattern = compile(r'^(?P<v1>\d+)-(?P<v2>\d+) (?P<letter>[a-z]): (?P<password>[a-z]+)$')

    def is_valid(datum: str) -> bool:
        match: Match = re_datum.search(datum)
        if match:
            groups: dict = match.groupdict()
            letter_count: int = sum(groups['letter'] == c for c in groups['password'])
            return int(groups['v1']) <= letter_count <= int(groups['v2'])
        else:
            raise ValueError(f'failed to parse entry: {repr(datum)}')

    n_valid: int = 0
    n_total: int = 0
    for value in d:
        n_valid += is_valid(value)
        n_total += 1
    return f'{n_valid} out of {n_total} passwords are valid'


@log_time(ignore_args_formatter)
def part2(d: List[str]) -> str:
    re_datum: Pattern = compile(r'^(?P<v1>\d+)-(?P<v2>\d+) (?P<letter>[a-z]): (?P<password>[a-z]+)$')

    def is_valid(datum: str) -> bool:
        match: Match = re_datum.search(datum)
        if match:
            groups: dict = match.groupdict()
            positions = (int(groups[pos]) for pos in ('v1', 'v2'))
            letter = groups['letter']
            password = groups['password']
            pass_length = len(groups['password'])
            count = sum(pass_length >= pos and password[pos - 1] == letter for pos in positions)
            return count == 1
        else:
            raise ValueError(f'failed to parse entry: {repr(datum)}')

    n_valid: int = 0
    n_total: int = 0
    for value in d:
        n_valid += is_valid(value)
        n_total += 1
    return f'{n_valid} out of {n_total} passwords are valid'


if __name__ == '__main__':
    data = read_input('d02_in.txt')
    part1(data)
    part2(data)
