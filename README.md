# Advent of Code

To learn more about advent of code, go to
[adventofcode.com](https://adventofcode.com/).

## Running Scripts

Scripts that don't import from other source files can be run as an executable.
You can run the day 1 challenges for AoC 2020 with:

```bash
$ python -m y2020.d01
```

To make it easier to select and run a script, you can use `python run.py` which
inquires about the year and day and then runs the above command and saves 
the output to an appropriately named `d*_out.txt` file.
