#!/usr/bin/env python
from datetime import datetime
from itertools import chain
from pathlib import Path
from subprocess import Popen, PIPE, STDOUT
from sys import executable, stdout
from typing import List

import inquirer


def inquire_choice(message: str, choices: List[str], default: str) -> str:
    return inquirer.prompt([
        inquirer.List('value', message=message, choices=choices, default=default)
    ])['value']


def day_module_list(year_path: Path) -> List[str]:
    return sorted(path.stem for path in chain(
        (p for p in year_path.glob('d[0-3][0-9]/') if p.is_dir()),
        (p for p in year_path.glob('d[0-3][0-9].py') if p.is_file())
    ))


def main():
    script_dir: Path = Path(__file__).parent
    now: datetime = datetime.now()
    year_dirs: List[str] = sorted(path.stem for path in script_dir.glob('y[0-9][0-9][0-9][0-9]') if path.is_dir())
    year_dir = f'y{now.year}'
    day_script = f'd{now.day}'
    while True:
        day_scripts: List[str] = []
        while len(day_scripts) == 0:
            year_dir = inquire_choice('Select year', year_dirs, year_dir)
            year_path = script_dir.joinpath(year_dir)
            day_scripts = day_module_list(year_path)
            if len(day_scripts) == 0:
                print(f'That year has no modules/scripts to run')
        day_script = inquire_choice('Select day', day_scripts, day_script)
        module = f'{year_dir}.{day_script}'
        output: Path = script_dir.joinpath(year_dir, day_script + '_out.txt')
        command = [executable, '-m', module]
        print(f'{" ".join(command)} > {output}\n')
        with output.open('w', encoding='utf8') as f_out:
            proc = Popen(command, stdout=PIPE, stderr=STDOUT)
            for line in proc.stdout:
                print(str(line, encoding='utf8'))
                stdout.write(str(line, encoding='utf8'))
                f_out.write(str(line, encoding='utf8'))
            proc.wait()
        print()


if __name__ == '__main__':
    main()
